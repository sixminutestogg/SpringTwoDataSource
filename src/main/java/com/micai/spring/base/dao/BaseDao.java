/**
 * 
 */
package com.micai.spring.base.dao;

/**
 * 公共的DAO处理对象，这个对象中包含了Hibernate的所有基本操作和对SQL的操作
 * 
 * @author ZhaoXinGuo
 * 
 * @email sxdtzhaoxinguo@163.com
 * 
 */
public interface BaseDao<T> {

	public T add(T t);

	public void update(T t);

	public void delete(int id);

	public T load(int id);

	public T load(String id);
	
}
