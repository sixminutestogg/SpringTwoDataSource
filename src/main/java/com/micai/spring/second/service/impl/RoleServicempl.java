/**
 * 
 */
package com.micai.spring.second.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.micai.spring.second.dao.RoleDao;
import com.micai.spring.second.po.Role;
import com.micai.spring.second.service.RoleService;

/**
 * @author ZhaoXinGuo
 *
 */
@Service("roleService")
public class RoleServicempl implements RoleService{

	@Autowired
	private RoleDao roleDao;

	public void saveRole(Role role) {
		roleDao.saveRole(role);
	}

	public List<Role> list() {
		return roleDao.list();
	}
	
}
