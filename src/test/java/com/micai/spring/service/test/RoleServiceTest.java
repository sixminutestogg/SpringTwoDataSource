/**
 * 
 */
package com.micai.spring.service.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.micai.spring.second.po.Role;
import com.micai.spring.second.service.RoleService;

/**
 * Spring的测试类编写方式 <br/>
 * 
 * @author ZhaoXinGuo
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class RoleServiceTest {

	private final Logger logger = LoggerFactory.getLogger(RoleServiceTest.class);
	
	@Autowired
	private RoleService roleSerice;
	
	@Test
	public void add(){
		Role role = new Role();
		role.setRoleName("管理员");
		role.setRoleDesc("拥有所有的权限");
		roleSerice.saveRole(role);
	}
	
	@Test
	public void list(){
		List<Role> list = roleSerice.list();
		logger.info("角色信息为：" + list + "-----------------");
	}
}
